package cn.liyicun.study.spring.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SampleController {

	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello World!" + System.nanoTime();
	}

	@RequestMapping(value = { "/" }, method = { RequestMethod.POST })
	@ResponseBody
	String home2() {
		return "wo shi get  post" + System.nanoTime();
	}

}
