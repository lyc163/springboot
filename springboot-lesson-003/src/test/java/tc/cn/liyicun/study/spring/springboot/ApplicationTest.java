package tc.cn.liyicun.study.spring.springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.liyicun.study.spring.springboot.Application;
import cn.liyicun.study.spring.springboot.service.TestService;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(Application.class)

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class }, properties = { "spring.profiles.active=test" })
public class ApplicationTest {

	@Autowired
	private TestService testService;

	@Test
	public void test() {
		System.out.println(testService.getHost());
	}

}
