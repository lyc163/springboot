package cn.liyicun.study.spring.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// @EnableAutoConfiguration() 这个不起作用, 知识点.
// @SpringBootApplication = (默认属性)@Configuration + @EnableAutoConfiguration +
// @ComponentScan。
public class Application {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
}
